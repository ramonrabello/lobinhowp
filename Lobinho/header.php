<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="ÜTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width", initial-scale="1.0">
    <link rel="shortcut icon" href="icon.ico">
    <title>Projeto Lobinho</title>
    <Link rel="Stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <Link rel="preconnect" href="https://fonts.googleapis.com">
    <Link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <Link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@300;400;500&family=Roboto:wght@300&display=swap" rel="stylesheet">
    <Link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <?php wp_head(); ?>
</head>
<body class="headerbody">
    <header>
        <div><a href="http://lobinho-wp.local/nossos-lobinhos/">Nossos Lobinhos</a></div>
        <a class="Header-Foto" href="http://lobinho-wp.local/"></a>
        <div><a href="http://lobinho-wp.local/pagina-quem-somos/">Quem Somos</a></div>
    </header>