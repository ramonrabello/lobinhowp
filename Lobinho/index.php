<?php 
// Template Name: Lista de lobinhos
?>
<?php get_header() ?>
    <main>
        <div class="Top-Bar">
            <div class="SearchBar-Container-Outside">
                <div class="SearchBar-Container-Inside">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>//resources/lupa.png" class="search-icon"></img>
                    <input>
                </div>
                <div class="Adicionar-Button"><form action="adicionar-lobinho.html"><button type="submit">+ Lobo</button></form></div>
            </div>
            <div class="CheckBox-Container">
                <div class="CheckBox-Conteudo">
                    <div class="Input"><input id="Adopted-CheckBox" type="checkbox"></div><div class="Texto">Ver lobinhos adotados</div>
                </div>
            </div>
        </div> 


        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                //Define our WP Query Parameters
                $the_query = new WP_Query(array('posts_per_page' => 4, 'paged' => $paged));
            ?>

            <?php $numero = 0 ?>

                <?php
                    //Start our WP Query
                    while($the_query -> have_posts()) : $the_query -> the_post();
                    //Display the Post Title with Hyperlink
                ?>

                <?php
                global $post;
                 if($post){ ?>
        <div class="Lobo-Lista">
            <div class="Lobo-Box" id="wrap">
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <!-- <div id="Foto1" class="Lobo-Foto"></div> -->
                        <figure class="Lobo-Foto">
                                <?php if(get_field('lobo_1_foto')): ?>
                                    <img src="<?php the_field('lobo_1_foto'); ?>">
                                <?php endif; ?>
                            </figure>
                    </div>
                </div>
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name-Button-Container">
                        <div id="Name1" class="Lobo-Name"><?php the_field('lobo_1_titulo') ?></div>
                        <div class="Adotar-Button"><button id="Adotar1">Adotar</button></div>
                        <div id="Hidden-ID1" class="Hidden-ID"></div>
                    </div>
                    <div id="Idade1" class="Lobo-Idade">Idade: <?php the_field('lobo_1_idade') ?></div>
                    <div class="Lobo-Texto-Container">
                        <div id="Texto1" class="Lobo-Texto"><?php the_field('lobo_1_descricao') ?>
                        </div>
                        <div id="Adopted-By1" class="Adopted-By"></div>
                    </div>
                </div>
            </div>
        </div>
                <?php }
                else{ ?>
                <div class="Lobo-Lista">
            <div class="Lobo-Box" id="wrap">
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <!-- <div id="Foto1" class="Lobo-Foto"></div> -->
                        <figure class="Lobo-Foto">
                                <?php if(get_field('lobo_1_foto')): ?>
                                    <img src="<?php the_field('lobo_1_foto'); ?>">
                                <?php endif; ?>
                            </figure>
                    </div>
                </div>
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name-Button-Container">
                        <div id="Name1" class="Lobo-Name"><?php the_field('lobo_1_titulo') ?></div>
                        <div class="Adotar-Button"><button id="Adotar1">Adotar</button></div>
                        <div id="Hidden-ID1" class="Hidden-ID"></div>
                    </div>
                    <div id="Idade1" class="Lobo-Idade">Idade: <?php the_field('lobo_1_idade') ?></div>
                    <div class="Lobo-Texto-Container">
                        <div id="Texto1" class="Lobo-Texto"><?php the_field('lobo_1_descricao') ?>
                        </div>
                        <div id="Adopted-By1" class="Adopted-By"></div>
                    </div>
                </div>
            </div>
            
        </div>
        

        
    </main>
    <div class="Pagination-Container">
        <div class="Pagination-Box">

        <?php }
                $numero++; ?>
                
                <?php
            //Repeat the process and reset once it hits the limit
            //backudkenfrei
                endwhile;
                   
                ?>
        </div>
    </div>
    <div class="paginationn"><?php 
    wp_reset_postdata();
                    echo paginate_links(array(
                        'base' => get_pagenum_link(1) . '%_%',
                        'format' => '/page/%#%',
                        'current' => 0,
                        'prev_text'    => __('« prev'),
                        'next_text'    => __('next »'),
                    ));
                    ?>
                </div>
<?php get_footer() ?>