<?php 
// Template Name: Lista de lobinhos
?>
<?php get_header() ?>
    <main>
        <div class="Top-Bar">
            <div class="SearchBar-Container-Outside">
                <div class="SearchBar-Container-Inside">
                    <img src="../resources/lupa.png" class="search-icon"></img>
                    <input>
                </div>
                <div class="Adicionar-Button"><form action="adicionar-lobinho.html"><button type="submit">+ Lobo</button></form></div>
            </div>
            <div class="CheckBox-Container">
                <div class="CheckBox-Conteudo">
                    <div class="Input"><input id="Adopted-CheckBox" type="checkbox"></div><div class="Texto">Ver lobinhos adotados</div>
                </div>
            </div>
        </div>
        <div class="Lobo-Lista">
            <div class="Lobo-Box" id="wrap">
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <div id="Foto1" class="Lobo-Foto"></div>
                    </div>
                </div>
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name-Button-Container">
                        <div id="Name1" class="Lobo-Name"><?php the_field('lobo_1_titulo') ?></div>
                        <div class="Adotar-Button"><button id="Adotar1">Adotar</button></div>
                        <div id="Hidden-ID1" class="Hidden-ID"></div>
                    </div>
                    <div id="Idade1" class="Lobo-Idade">Idade: XX anos</div>
                    <div class="Lobo-Texto-Container">
                        <div id="Texto1" class="Lobo-Texto">Não obstante, o surgimento do comércio virtual faz parte de um processo 
                            de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis 
                            envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de 
                            gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das 
                            variáveis envolvidas.
                        </div>
                        <div id="Adopted-By1" class="Adopted-By"></div>
                    </div>
                </div>
            </div>
            <div class="Lobo-Box" id="reverse">
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name-Button-Container">
                        <div id="Name2" class="Lobo-Name">Nome do Lobo</div>
                        <div class="Adotar-Button"><button id="Adotar2">Adotar</button></div>
                        <div id="Hidden-ID2" class="Hidden-ID"></div>
                    </div>
                    <div id="Idade2" class="Lobo-Idade">Idade: XX anos</div>
                    <div class="Lobo-Texto-Container">
                        <div id="Texto2" class="Lobo-Texto">Não obstante, o surgimento do comércio virtual faz parte de um processo 
                            de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis 
                            envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de 
                            gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das 
                            variáveis envolvidas.
                        </div>
                        <div id="Adopted-By2" class="Adopted-By"></div>
                    </div>
                </div>
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <div id="Foto2" class="Lobo-Foto"></div>
                    </div>
                </div>
            </div>
            <div class="Lobo-Box" id="wrap">
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <div id="Foto3" class="Lobo-Foto"></div>
                    </div>
                </div>
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name-Button-Container">
                        <div id="Name3" class="Lobo-Name">Nome do Lobo</div>
                        <div class="Adotar-Button"><button id="Adotar3">Adotar</button></div>
                        <div id="Hidden-ID3" class="Hidden-ID"></div>
                    </div>
                    <div id="Idade3" class="Lobo-Idade">Idade: XX anos</div>
                    <div class="Lobo-Texto-Container">
                        <div id="Texto3" class="Lobo-Texto">Não obstante, o surgimento do comércio virtual faz parte de um processo 
                            de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis 
                            envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de 
                            gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das 
                            variáveis envolvidas.
                        </div>
                        <div id="Adopted-By3" class="Adopted-By"></div>
                    </div>
                </div>
            </div>
            <div class="Lobo-Box" id="reverse">
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name-Button-Container">
                        <div id="Name4" class="Lobo-Name">Nome do Lobo</div>
                        <div class="Adotar-Button"><button id="Adotar4">Adotar</button></div>
                        <div id="Hidden-ID4" class="Hidden-ID"></div>
                    </div>
                    <div id="Idade4" class="Lobo-Idade">Idade: XX anos</div>
                    <div class="Lobo-Texto-Container">
                        <div id="Texto4" class="Lobo-Texto">Não obstante, o surgimento do comércio virtual faz parte de um processo 
                            de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis 
                            envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de 
                            gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do 
                            comércio virtual faz parte de um processo de gerenciamento do levantamento das 
                            variáveis envolvidas.
                        </div>
                        <div id="Adopted-By4" class="Adopted-By"></div>
                    </div>
                </div>
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <div id="Foto4" class="Lobo-Foto"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Pagination-Container">
            <div class="Pagination-Box">
                <button id="Previous-Page"><<</button>
                <a id="number-pagination1">1</a>
                <a id="number-pagination2">2</a>
                <a id="number-pagination3">3</a>
                <a id="number-pagination4">4</a>
                <a id="number-pagination5">5</a>
                <div>...</div>
                <button id="Next-Page">>></button>
            </div>
        </div>
    </main>
<?php get_footer() ?>